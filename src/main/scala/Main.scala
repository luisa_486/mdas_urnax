import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.{HttpEntity, _}
import akka.http.scaladsl.unmarshalling.Unmarshal
import mdas.paradigmas.urnax.application._
import mdas.paradigmas.urnax.domain.{VotationCentersRepository, VotationsRepository, Voter, VotersRepository}
import mdas.paradigmas.urnax.infraestructure.{VotationCentersRepositoryInMemory, VotationsRepositoryInMemory, VotersRepositoryInMemory}

import java.text.SimpleDateFormat
import java.util.Date
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.StdIn
import scala.language.postfixOps


object Main extends App {
  implicit val system: ActorSystem[Nothing] = ActorSystem(Behaviors.empty, "lowlevel")
  // needed for the future map/flatmap in the end
  implicit val executionContext: ExecutionContext = system.executionContext

  //elements of VotationCenter
  val centersRepository: VotationCentersRepository = VotationCentersRepositoryInMemory
  val createCenter: VotationCenterCreate = new VotationCenterCreate(centersRepository)
  val getAllCenters: VotationCenterGetAll = new VotationCenterGetAll(centersRepository)

  //elements of VotationProcess
  val votationsRepository: VotationsRepository = VotationsRepositoryInMemory
  val createVotation: VotationCreate = new VotationCreate(votationsRepository)
  val getAllVotations: VotationGetAll = new VotationGetAll(votationsRepository)
  val votationFindByIndex: VotationFindByIndex = new VotationFindByIndex(votationsRepository)

  //elements of Voter
  val voterRepository: VotersRepository = VotersRepositoryInMemory
  val createVoter: VoterCreate = new VoterCreate(voterRepository)
  val getAllVoters: VoterGetAll = new VoterGetAll(voterRepository)
  val voterFindByIdNumber: VoterFindByIdNumber = new VoterFindByIdNumber(voterRepository)

  val requestHandler: HttpRequest => HttpResponse = {
    //creating votation center
    case HttpRequest(POST, Uri.Path("/center"), _, entity, _) =>
      try {
        val index = createCenter execute parser(entity)
        HttpResponse(entity = HttpEntity(
          ContentTypes.`application/json`,
          s"Votation center create successfully with index $index\n"))
      } catch {
        case e: Exception => HttpResponse(500, entity = s"ups: ${e.getMessage}")
      }

    case HttpRequest(GET, Uri.Path("/center"), _, _, _) =>
      HttpResponse(200, entity = getAllCenters.execute.toString())

    //creating votation process
    case HttpRequest(POST, Uri.Path("/votation"), _, entity, _) =>
      try {
        val index = createVotation execute parser(entity)
        HttpResponse(entity = HttpEntity(
          ContentTypes.`application/json`,
          s"Votation Process create successfully with index $index\n"))
      } catch {
        case e: Exception => HttpResponse(500, entity = s"ups: ${e.getMessage}")
      }

    case HttpRequest(GET, Uri.Path("/votation"), _, _, _) =>
      HttpResponse(200, entity = getAllVotations.execute.toString)

    //creating voter
    case HttpRequest(POST, Uri.Path("/voter"), _, entity, _) =>
      try {
        val (index, nearestVotationCenter) = createVoter.execute(parser(entity), centersRepository)

        HttpResponse(entity = HttpEntity(
          ContentTypes.`application/json`,
          s"Voter create successfully with index $index\n The votation Center Assigned is: $nearestVotationCenter"))
      } catch {
        case e: Exception => HttpResponse(500, entity = s"ups: ${e.getMessage}")
      }

    case HttpRequest(GET, Uri.Path("/voters"), _, _, _) =>
      HttpResponse(200, entity = getAllVoters.execute().toString)

    //check voter identity
    case HttpRequest(POST, Uri.Path("/check_voter"), _, entity, _) =>
      try {
        val voter = voterFindByIdNumber execute parser(entity)("idNumber")
        HttpResponse(entity = HttpEntity(
          ContentTypes.`application/json`,
          s"Voter find with parameters: $voter\n"))
      } catch {
        case e: Exception => HttpResponse(200, entity = s"This Voter Does Not Exists:  ${e.getMessage}")
      }

    //Activate the Sending of Messages
    case HttpRequest(POST, Uri.Path("/activate_message_send"), _, entity, _) =>
      try {
        val votation = votationFindByIndex execute parser(entity)("indexVotation").toInt
        //getting days from now until the votation day
        val daysToWait = daysUntilNow(votation.date)
        //if the day of votation is today, we send messages
        if (daysToWait == 0) sendMessage(getAllVoters.execute())
        //else the messages will be senden in the future
        else {
          val messagesSended = Await.result(sendMessage(getAllVoters.execute()), daysToWait days)
          println(s"We sended $messagesSended messages")
        }

        HttpResponse(entity = HttpEntity(
          ContentTypes.`application/json`,
          s"The messages will be sending in ${votation.date}"))
      } catch {
        case e: Exception => HttpResponse(200, entity = s"An error has ocurred:  ${e.getMessage}")
      }

    case r: HttpRequest =>
      r.discardEntityBytes() // important to drain incoming HTTP Entity stream
      HttpResponse(404, entity = "Unknown resource!")
  }

  val port = 8080
  val bindingFuture = Http().newServerAt("localhost", port).bindSync(requestHandler)
  println(s"Server online at http://localhost:$port/\nPress RETURN to stop...")
  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done

  def parser(entity: HttpEntity): Map[String, String] =
    Await.result(Unmarshal(entity).to[String], 300 millis)
      .split('&')
      .map { keyAndValue: String =>
        val vector = keyAndValue.split('=')
        vector(0) -> vector(1)
      }
      .toMap

  //sending message in the election day
  def sendMessage(voters: List[Voter]): Future[Int] = Future {
    // assume some long running database operation
    voters.foreach(voter =>
      println(s"${voter.name}! Today is the election day. Do yo have to vote in the center: ${voter.votationCenter.name}")
    )
    voters.length
  }

  //getting days until now
  def daysUntilNow(day: Date): Int = {
    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    val today = formatter.parse(formatter.format(new Date()))
    day.compareTo(today)
  }
}
