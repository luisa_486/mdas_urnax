package mdas.paradigmas.urnax.application

import mdas.paradigmas.urnax.domain.{Coordinates, VotationCenter, VotationCentersRepository}

class VotationCenterByCoordinates (private val repository: VotationCentersRepository) {
  def execute(coordinates: Coordinates): VotationCenter = repository findNearest coordinates
}
