package mdas.paradigmas.urnax.application

import mdas.paradigmas.urnax.domain.{Votation, VotationsRepository}

class VotationFindByIndex (private val repository: VotationsRepository) {
  def execute(id: Int): Votation = repository findByIndex id
}
