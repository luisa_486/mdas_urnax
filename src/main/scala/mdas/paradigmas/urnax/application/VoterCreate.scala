package mdas.paradigmas.urnax.application

import mdas.paradigmas.urnax.domain.{VotationCenter, VotationCentersRepository, Voter, VotersRepository}

import java.util.concurrent.TimeUnit
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

class VoterCreate(private val repository: VotersRepository) {
  implicit val baseTime: Long = System.currentTimeMillis
  def execute(map: Map[String, String], centers: VotationCentersRepository): (Int, VotationCenter) = {
    if (validateVoter(map("idNumber"))) repository.save(Voter(map), centers)
    else throw new InvalidVoterException(s"This Voter $map does not exist in National Register Citizens")
  }

  def validateVoter(idNumber: String): Boolean = {
    val maxWaitTime: FiniteDuration = Duration(5, TimeUnit.SECONDS)
    Await.result(connectingWithSystem(idNumber), maxWaitTime)
  }
  //In this function we simulate the connection with an external National Registry. Any idNumber whose last digit is
  // greater than or equal to five will be fine!
  def connectingWithSystem(idNumber: String): Future[Boolean] = Future {
    Thread.sleep(30)
    val number = idNumber takeRight 1
    number.toInt >= 5
  }
}
