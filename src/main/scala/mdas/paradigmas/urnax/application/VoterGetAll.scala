package mdas.paradigmas.urnax.application

import mdas.paradigmas.urnax.domain.{Voter, VotersRepository}

class VoterGetAll(private val repository: VotersRepository) {
  def execute(): List[Voter] = repository.getAll
}
