package mdas.paradigmas.urnax.application

import mdas.paradigmas.urnax.domain.{VotationCenter, VotationCentersRepository}

class VotationCenterGetAll(private val repository: VotationCentersRepository) {
  def execute: List[VotationCenter] = repository.getAll
}
