package mdas.paradigmas.urnax.application

class InvalidVoterException(motive: String) extends Exception(motive)
