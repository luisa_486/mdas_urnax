package mdas.paradigmas.urnax.application

import mdas.paradigmas.urnax.domain.{Votation, VotationsRepository}

class VotationCreate(private val repository: VotationsRepository) {
  def execute(map: Map[String, String]): Int = repository save Votation(map)
}
