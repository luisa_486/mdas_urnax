package mdas.paradigmas.urnax.application

import mdas.paradigmas.urnax.domain.{VotationCenter, VotationCentersRepository}

class VotationCenterCreate(private val repository: VotationCentersRepository) {
  def execute(map: Map[String, String]): Int = repository save VotationCenter(map)
}

