package mdas.paradigmas.urnax.application

import mdas.paradigmas.urnax.domain.{Voter, VotersRepository}

class VoterFindByIdNumber(private val repository: VotersRepository) {
  def execute(id: String): Voter = repository findByIdNumber  id
}
