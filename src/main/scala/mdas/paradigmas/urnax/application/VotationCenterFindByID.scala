package mdas.paradigmas.urnax.application

import mdas.paradigmas.urnax.domain.VotationCentersRepository

class VotationCenterFindByID(private val repository: VotationCentersRepository) {
  def execute(id: Int): Unit = repository findByID id
}
