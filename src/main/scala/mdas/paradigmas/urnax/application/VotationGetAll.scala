package mdas.paradigmas.urnax.application

import mdas.paradigmas.urnax.domain.{Votation, VotationsRepository}

class VotationGetAll(private val repository: VotationsRepository) {
  def execute: List[Votation] = repository.getAll
}
