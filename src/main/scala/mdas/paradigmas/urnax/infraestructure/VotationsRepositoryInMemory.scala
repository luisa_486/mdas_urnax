package mdas.paradigmas.urnax.infraestructure

import mdas.paradigmas.urnax.domain.{InvalidDateException, Votation, VotationsRepository}

import java.text.SimpleDateFormat
import java.util.Date


object VotationsRepositoryInMemory extends VotationsRepository{
  private val votationProcess: scala.collection.mutable.ArrayBuffer[Votation] = new scala.collection.mutable.ArrayBuffer[Votation]

  override def getAll: List[Votation] = votationProcess.toList

  override def save(votation: Votation): Int = if (votationProcess contains votation)
    throw new VotationAlreadyExistsException(s"Already Exists a Votation Process with this parameters: ${votation}")
  else {
    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    val today = formatter.parse(formatter.format(new Date()))
    if(votation.date.compareTo(today)>=0){
      votationProcess addOne votation
      votationProcess indexOf votation
    }
    else throw new InvalidDateException(s"The date of the voting process cannot be less than the current date")
  }

  override def findByIndex(id: Int): Votation = votationProcess(id)
}
