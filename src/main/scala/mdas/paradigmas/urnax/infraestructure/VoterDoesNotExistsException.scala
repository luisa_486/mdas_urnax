package mdas.paradigmas.urnax.infraestructure

class VoterDoesNotExistsException(motive: String) extends Exception(motive)
