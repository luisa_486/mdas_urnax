package mdas.paradigmas.urnax.infraestructure

import mdas.paradigmas.urnax.domain.{Coordinates, VotationCenter, VotationCentersRepository}

object VotationCentersRepositoryInMemory extends VotationCentersRepository {
  private val votationCenters: scala.collection.mutable.ArrayBuffer[VotationCenter] = new scala.collection.mutable.ArrayBuffer[VotationCenter]

  override def getAll: List[VotationCenter] = votationCenters.toList

  override def findByID(id: Int): VotationCenter = votationCenters(id)

  override def save(votationCenter: VotationCenter): Int = if (votationCenters contains votationCenter)
    throw new VotationCenterAlreadyExistsException(s"Already Exists a Votation Center with this parameters: ${votationCenter}")
  else {
    votationCenters addOne votationCenter
    votationCenters indexOf votationCenter
  }

  override def findNearest(coordinates: Coordinates): VotationCenter =
    votationCenters.minBy { center => coordinates distance center.coordinates }
}
