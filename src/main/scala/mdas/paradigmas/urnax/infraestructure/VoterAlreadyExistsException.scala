package mdas.paradigmas.urnax.infraestructure

class VoterAlreadyExistsException(motive: String) extends Exception(motive)
