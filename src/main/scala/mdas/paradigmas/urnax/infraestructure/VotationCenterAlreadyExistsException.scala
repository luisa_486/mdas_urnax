package mdas.paradigmas.urnax.infraestructure

class VotationCenterAlreadyExistsException(motive: String) extends Exception(motive)
