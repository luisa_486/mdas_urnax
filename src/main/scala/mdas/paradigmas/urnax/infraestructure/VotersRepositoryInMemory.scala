package mdas.paradigmas.urnax.infraestructure

import mdas.paradigmas.urnax.domain.{VotationCenter, VotationCentersRepository, Voter, VotersRepository}

object VotersRepositoryInMemory extends VotersRepository {
  private val voters: scala.collection.mutable.ArrayBuffer[Voter] = new scala.collection.mutable.ArrayBuffer[Voter]

  override def getAll: List[Voter] = voters.toList

  override def save(voter: Voter, centers: VotationCentersRepository): (Int, VotationCenter) = {
    try {
      findByIdNumber(voter.idNumber)
      throw new VoterAlreadyExistsException(s"Already Exists a Voter with this idNumber: ${voter.idNumber}")
    } catch {
      case e: Exception =>
        voters addOne voter
        val index = voters indexOf voter
        (index, assignNearestVotationCenter(index, centers))
    }
  }

  override def findByIdNumber(idNumber: String): Voter = voters.find(_.idNumber == idNumber).getOrElse(throw new VoterDoesNotExistsException("Not found"))

  override def findByIndex(index: Int): Voter = voters(index)

  override def assignNearestVotationCenter(voterId: Int, centers: VotationCentersRepository): VotationCenter = {
    val voter = findByIndex(voterId)
    val center = centers.findNearest(voter.direction)
    voter.votationCenter = center
    center
  }
}
