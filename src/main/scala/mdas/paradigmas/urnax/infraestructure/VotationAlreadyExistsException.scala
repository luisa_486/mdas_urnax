package mdas.paradigmas.urnax.infraestructure

class VotationAlreadyExistsException(motive: String) extends Exception(motive)
