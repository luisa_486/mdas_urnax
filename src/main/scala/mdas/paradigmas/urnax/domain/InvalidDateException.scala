package mdas.paradigmas.urnax.domain

class InvalidDateException(motive: String) extends Exception(motive)
