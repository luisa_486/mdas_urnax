package mdas.paradigmas.urnax.domain

import scala.math.{pow, sqrt}

case class Coordinates(longitude: Float, latitude: Float) {
  def distance(other: Coordinates): Double = sqrt(pow(longitude - other.longitude, 2) + pow(latitude - other.latitude, 2))
}

object Coordinates {
  def apply(map: Map[String, String]): Coordinates =
    new Coordinates(
      longitude = getValue(map, "longitude"),
      latitude = getValue(map, "latitude")
    )

  private def getValue(map: Map[String, String], key: String) = map.get(key) match {
    case Some(value) => try
      value.toFloat
    catch {
      case e: Exception => throw new InvalidFloatException(s"For key: $key, need a float, not a $value")
    }
    case None => throw new NotFoundKeyException(s"key: $key not defined, only defined $map")
  }
}
