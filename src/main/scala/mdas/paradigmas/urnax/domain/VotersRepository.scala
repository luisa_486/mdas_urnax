package mdas.paradigmas.urnax.domain

trait VotersRepository {
  def getAll: List[Voter]
  def save(voter: Voter, centers: VotationCentersRepository): (Int, VotationCenter)
  def findByIdNumber(idNumber: String): Voter
  def findByIndex(index: Int): Voter
  def assignNearestVotationCenter(voterId: Int, centers: VotationCentersRepository) : VotationCenter
}
