package mdas.paradigmas.urnax.domain

trait VotationCentersRepository {
  def getAll: List[VotationCenter]
  def findByID(id: Int): VotationCenter
  def save(votationCenter: VotationCenter): Int
  def findNearest(coordinates: Coordinates): VotationCenter
}
