package mdas.paradigmas.urnax.domain

import java.text.SimpleDateFormat
import java.util.Date

case class Votation(name: String, date: Date)

object Votation {
  val dateformat = new SimpleDateFormat("yyyy-MM-dd")

  def apply(map: Map[String, String]): Votation =
    new Votation(
      name = getValue(map, "name"),
      date = getDate(getValue(map, "date"))
    )

  private def getDate(date: String): Date =
    try
      dateformat.parse(date)
    catch {
      case e: Exception => throw new InvalidDateException("The field date, is not a valid Date Value. Must be 'yyyy-MM-dd'")
    }

  private def getValue(map: Map[String, String], key: String) = map.get(key) match {
    case Some(value) => value
    case None => throw new NotFoundKeyException(s"key: $key not defined, only defined $map")
  }
}
