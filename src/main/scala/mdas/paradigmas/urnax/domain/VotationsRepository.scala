package mdas.paradigmas.urnax.domain

trait VotationsRepository {
  def getAll: List[Votation]
  def save(votationProcess: Votation): Int
  def findByIndex(id: Int): Votation
}
