package mdas.paradigmas.urnax.domain

class NotFoundKeyException(motive: String) extends Exception(motive)
