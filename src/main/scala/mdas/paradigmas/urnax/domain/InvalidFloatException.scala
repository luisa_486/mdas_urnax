package mdas.paradigmas.urnax.domain

class InvalidFloatException(motive: String) extends Exception(motive)
