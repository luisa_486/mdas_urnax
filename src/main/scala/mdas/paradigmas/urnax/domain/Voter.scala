package mdas.paradigmas.urnax.domain

case class Voter(idNumber:String, name:String, lastName:String, direction:Coordinates,
                 var votationCenter: VotationCenter = null)

object Voter {
  def apply(map: Map[String, String]): Voter =
    new Voter(
      idNumber = getValue(map, "idNumber"),
      name = getValue(map, "name"),
      lastName = getValue(map, "lastName"),
      direction = Coordinates(map)
    )

  private def getValue(map: Map[String, String], key: String) = map.get(key) match {
    case Some(value) => value
    case None => throw new NotFoundKeyException(s"key: $key not defined, only defined $map")
  }
}
