package mdas.paradigmas.urnax.domain

case class VotationCenter(name: String, coordinates: Coordinates)

object VotationCenter {
  def apply(map: Map[String, String]): VotationCenter =
    new VotationCenter(
      name = getValue(map, "name"),
      coordinates = Coordinates(map)
    )

  private def getValue(map: Map[String, String], key: String) = map.get(key) match {
    case Some(value) => value
    case None => throw new NotFoundKeyException(s"key: $key not defined, only defined ${map}")
  }
}
