# MDAS URNAX
## _A simulator made in SCALA for the company e-democracy: URNAX_

[![N|Solid](https://www.data-blogger.com/wp-content/uploads/2016/07/scala-logo.png)](https://www.data-blogger.com/wp-content/uploads/2016/07/scala-logo.png)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This is a project carried out in the MDAS for the electronic voting process of the URNAX company. All functionalities are accessed through API Rest. To test it you need:

- Docker installation (optional, if you have Intellij you can run the program from the IDE or simply by placing yourself in the project folder and executing 'gradlew run')
- Yes... we use Scala with Gradle, Akka and jib
- ✨ Its Magic ✨



## FEATURES

### 1. Votation Centers
    With this feature, you can create and consult the voting centers, which are the places where people cast their vote. 
     
    # Create Votation Centers #
     To create a Votation Center, you only need to have its name and coordinates: Latitude and Longitude. Our system will be in charge of assigning the ** index ** that corresponds to the center in an orderly manner, and with this index you can perform the other actions of the system
    
    - link: http://localhost:8080/center
    - Method: POST
    - Parameters: 
        - name
        - latitude
        - longitude
        
    # Consult Votation Centers Created #
     With this functionality you can list all the votation centers that you have created:
    
    - link: http://localhost:8080/center
    - Method: GET

### 2. Votation Process
    With this feature, you can create and consult the voting processes created. These are the elections as such that will represent the voting process in which a Voter participates.
     
    # Create Voting Process #
    To create a Voting Process you must have the name, the date on which the voting will take place (yyyy-MM-dd) and the list of options to which users will have access to vote. Remember that the system will validate that you do not try to create a Voting Process with a date before the current one :)
    
    - link: http://localhost:8080/votation
    - Method: POST
    - Parameters: 
        - name
        - date
        - opctions (list of String separates by '|')
        
    # Consult Voting Processes Created #
     With this functionality you can list all the voting processes that you have created:
    
    - link: http://localhost:8080/votation
    - Method: GET

### 3. Voters
    With this feature, you can create and consult created voters. These are the people who exercise the vote, for a voting process, in a voting center. 
     
    # Create Voters #
     To create a voter you must enter the idNumber (your identification number), your name, surname and address in the form of coordinates. With the address, the system will automatically assign the Voting Center closest to the voter :)
     ## NOTE: We have simulated the connection to a National Citizen Registry system, which verifies that a citizen exists in order to create it. For this we use a Future :) in our fictitious validation, you can only create voters whose last digit of the idNumber is equal to or greater than 5:) ###
    
    - link: http://localhost:8080/voter
    - Method: POST
    - Parameters: 
        - idNumber
        - name
        - lastName
        - longitude
        - latitude
        
    # Consult Created Voters #
     With this functionality you can list all the voters that you have created:
    
    - link: http://localhost:8080/voters
    - Method: GET
    
     # Verify the Identity of a Voter #
     With this functionality, those in charge of the voting centers, will be able to consult by means of the idNumber, if a voter is valid to exercise the vote.
    
    - link: http://localhost:8080/check_voter
    - Method: POST
    - Parameters: 
        - idNumber

### 3. Activate the sending of messages on voting day
    With this feature, after having created and confirmed a voting process, you will be able to activate the message sending function. Which runs a future thread where on the day of voting, voters will receive a message with their assigned information center (you can see the results of this sending on your console)
    
    - link: http://localhost:8080/activate_message_send
    - Method: POST
    - Parameters: 
        - indexVotation (index of the votation process created, you can check it in point 2 of this list of features)

## Tech

Some of the technologies we use to create this app are:

- [IntellijIDEA](https://www.jetbrains.com/idea/) - Editor
- [Graddle](https://gradle.org/) - Build Tool
- [Scala](https://www.scala-lang.org/) - Languaje
- [Akka](https://akka.io/) - toolkit for building highly concurrent, distributed, and resilient message-driven applications for Java and Scala.
- [Jib](https://cloud.google.com/java/getting-started/jib) - builds containers without using a Dockerfile or requiring a Docker installation. You can use Jib in the Jib plugins for Maven or Gradle, or you can use the Jib Java library.
- [Docker](https://www.docker.com/) - We use Docker Contairners!


## Installation

With Docker, go to the main folder and exec:
```sh
docker run -ti mdas_urnax:1.0-SNAPSHOT
```
With graddle:

```sh
cd folderProject
gradle run
```

## POSTMAN TO THE RESCUE

You can use the file
```sh
MDAS_URNAX.postman_collection.json
```
And test our APP!